package com.danylko.view;

public interface Printable<K, V> {
    void print();
}
