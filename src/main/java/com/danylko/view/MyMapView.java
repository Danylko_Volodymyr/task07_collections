package com.danylko.view;

import com.danylko.binary.BinaryTree;

import java.util.*;

public class MyMapView<K, V> {
    BinaryTree<K, V> tree = new BinaryTree<>();

    // List<Methods> l;
    private enum Methods {
        Print("  1 - print Appliance List"),
        Put("  2 - Put"),
        Get("  3 - get"),
        Remove("  4 - remove"),
        Exit("  Q - exit");
        private String code;

        Methods(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    /**
     *
     */
    public MyMapView() {
//        l = new LinkedList<Methods>();
//        l.add(Methods.Print);
//        l.add(Methods.Put);
//        l.add(Methods.Get);
//        l.add(Methods.Remove);
//        l.add(Methods.Exit);

//        controller = new MyController();
        menu = new LinkedHashMap<>();
        menu.put("1", Methods.Print.getCode());
        menu.put("2", Methods.Put.getCode());
        menu.put("3", Methods.Get.getCode());
        menu.put("4", Methods.Remove.getCode());
        menu.put("Q", Methods.Exit.getCode());

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }


    /**
     *
     */
    private void pressButton1() {
        System.out.println(tree.toString());
    }

    /**
     *
     */
    private void pressButton2() {
        System.out.print("Please input Key:");
        K key = (K) input.next();
        System.out.print("Please input Value:");
        V value = (V) input.next();
        tree.put(key, value);
    }

    /**
     *
     */
    private void pressButton3() {
        System.out.print("Please input Key:");
        K key = (K) input.next();
        System.out.println(tree.get(key));
    }

    /**
     *
     */
    private void pressButton4() {
        System.out.print("Please input Key:");
        K key = (K) input.next();
        tree.remove(key);
    }

    /**
     *
     */
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     *
     */
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } while (!keyMenu.equals("Q"));
    }
}
